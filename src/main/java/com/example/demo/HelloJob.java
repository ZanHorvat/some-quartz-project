package com.example.demo;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job {

    public void execute(JobExecutionContext jExeCtx){
        System.out.println("TestJob run successfully...");
    }

}
